
import './epos-print-4.1.0'
import moment from 'moment'
// import './lib/epos-2.3.0.js'

const printerIp = '10.0.110.110'

module.exports = function (invitation) {
  console.log(invitation)
  let builder = new epson.ePOSBuilder()
  builder.addLayout(builder.LAYOUT_RECEIPT, 580, 0, 15, -15, 25, 0)
  // initialize (alphanumeic mode, smoothing)
  builder.addTextLang('en')
  builder.addTextSmooth(1)

  // paper control
  builder.addFeedPosition(builder.FEED_CURRENT_TOF)

  // paper control for first print
  builder.addFeedPosition(builder.FEED_NEXT_TOF)
  // Cristopher Variables 
  // const calculatePageArea = (invitation.pmses.length * 7 * 25) + (invitation.giftses.length * 25) + (invitation.userInvitations.length * 25)
  //const calculatePageArea = (invitation.giftses.length * 25) + (invitation.userInvitations.length * 25)
  const calculatePageArea = 300
  let currentLine = 500
  const addLine = 35
  const addSection = 60
  // start page mode
  builder.addPageBegin()
  //                        ancho, largo
  // ----------------- x, y, width, height. format of the page
  // let height =  8
  let heightBox1 = 300
  let heightBox2 = 500
  let heightBox3 = 800
  let heightBox4 = 1200
  builder.addPageArea(0, 0, 575, 1200 + calculatePageArea)
  /* - width- */
  let standar = 25
  // let roomNumber = '123034';
  let wPos_2 = 50, wPos_3 = 100, wPos_4 = 150, wPos_5 = 200, wPos_6 = 250, wPos_7 = 300
  let wPos_8 = 350, wPos_9 = 400, wPos_10 = 450, wPos_11 = 500, wPos_12 = 550

  /* - height - */
  let hPos_2 = 50, hPos_3 = 100, hPos_4 = 150, hPos_5 = 200, hPos_6 = 250, hPos_7 = 300, hPos_8 = 350, hPos_9 = 400
  let hPos_10 = 450, hPos_11 = 500, hPos_12 = 550, hPos_13 = 600, hPos_14 = 650, hPos_15 = 700, hPos_16 = 750, hPos_17 = 800
  let hPos_18 = 850, hPos_19 = 900, hPos_20 = 950, hPos_21 = 1000, hPos_22 = 1050, hPos_23 = 1100, hPos_24 = 1150, hPos_25 = 1200
  let hPos_26 = 1250, hPoss_27 = 1300, hPos_28 = 1350, hPos_29 = 1400, hPos_30 = 1450

  /* -- Box1 -- */

  builder.addPagePosition(0, hPos_2)
  builder.addTextDouble(true, true).addText('PRIVILEGE CLUB')
  builder.addTextDouble(false, false)

  builder.addPagePosition(wPos_9, 0)

  let data = 'colorapp_colorapp_colorapp_colorapp' // QR_Content
  builder.addSymbol(data, builder.SYMBOL_QRCODE_MODEL_2, builder.LEVEL_H)

  builder.addPagePosition(wPos_2 - standar, hPos_2 + standar)
  builder.addText('Bahia Principe')

  builder.addPagePosition(0, hPos_4 - 30)
  builder.addTextDouble(true, false).addText(`${invitation.pms[0].firstName} ` + `${invitation.pms[0].lastName}`)
  builder.addTextDouble(false, false)
  builder.addPagePosition(0, hPos_4)
  builder.addText('Habitacion:')
  builder.addPagePosition(wPos_4 + standar, hPos_4)
  builder.addText(invitation.pms[0].roomNumber)

  builder.addPagePosition(0, hPos_6 - standar)
  builder.addText('CITA')
  builder.addPagePosition(0, hPos_6 - 18)
  builder.addText('_______________________________________________')

  /* -- Box2 -- */
  // let now = new Date()
  // let date = now.toDateString();
  // let time = now.toTimeString().slice(0, 8);

  builder.addPagePosition(0, hPos_7 - standar);
  builder.addText('Fecha')
  builder.addPagePosition(0, hPos_7 + 5)
  builder.addText(moment(invitation.appointmentDate).format('YYYY-MM-DD'))

  builder.addPagePosition(wPos_6, hPos_7 - standar)
  builder.addText('H. Entrada')
  builder.addPagePosition(wPos_6, hPos_7 + 5)
  builder.addText(moment(invitation.pms[0].checkIn).format('HH:mm'))

  builder.addPagePosition(wPos_9, hPos_7 - standar)
  builder.addText('Hora Pick-UP')
  builder.addPagePosition(wPos_9, hPos_7 + 5)
  builder.addText(moment(invitation.pickUpTime).format('HH:mm'))

  builder.addPagePosition(0, hPos_8)
  builder.addText('Pick-UP')
  builder.addPagePosition(0, hPos_8 + 30)
  builder.addText(invitation.pickUp)

  builder.addPagePosition(wPos_5 - standar, hPos_8)
  builder.addText('Adultos')
  builder.addPagePosition(wPos_5 - standar, hPos_8 + 30)
  builder.addText(invitation.adultsQty)

  builder.addPagePosition(wPos_7 - standar, hPos_8)
  builder.addText('Niños')
  builder.addPagePosition(wPos_7 - standar, hPos_8 + 30)
  builder.addText(invitation.childrenQty)

  builder.addPagePosition(wPos_9, hPos_8)
  builder.addText('Idioma')
  builder.addPagePosition(wPos_9, hPos_8 + 30)
  builder.addText(invitation.language.description)

  builder.addPagePosition(0, hPos_10)
  /*  builder.addText('PROSPECTO(S)')
   builder.addPagePosition(0, hPos_10 + 7)
   builder.addText('_______________________________________________') */

  /* -- Box3 -- */

  /* for (let i = 0; i < invitation.pmses.length; i++) {
    builder.addPagePosition(0, currentLine)
    builder.addText('Nombre Completo')
    builder.addPagePosition(0, currentLine + addLine)
    builder.addText(`${invitation.pmses[i].prospectFirstName} ` + invitation.pmses[i].prospectLastName)
    builder.addPagePosition(wPos_8, currentLine)
    builder.addText('Pais')
    builder.addPagePosition(wPos_8, currentLine + addLine)
    builder.addText(invitation.pmses[i].country)

    currentLine += 85

    builder.addPagePosition(0, currentLine)
    builder.addText('Habitacion')
    builder.addPagePosition(0, currentLine + addLine)
    builder.addText(invitation.pmses[i].roomNumber)
    builder.addPagePosition(wPos_4, currentLine)
    builder.addText('Check-IN')
    builder.addPagePosition(wPos_4, currentLine + addLine)
    builder.addText(invitation.pmses[i].checkIn)
    builder.addPagePosition(wPos_9 - standar, currentLine)
    builder.addText('Check-OUT')
    builder.addPagePosition(wPos_9 - standar, currentLine + addLine)
    builder.addText(invitation.pmses[i].checkOut)
 
    currentLine += 85

    builder.addPagePosition(0, currentLine)
    builder.addText('Hotel')
    builder.addPagePosition(0, currentLine + addLine)
    builder.addText(invitation.pmses[i].hotel)
 
    builder.addPagePosition(wPos_6 + standar, currentLine)
    builder.addText('TourOperator')
    builder.addPagePosition(wPos_6 + standar, currentLine + addLine)
    builder.addText(invitation.pmses[i].tourOperator)
    currentLine += 85
    builder.addPagePosition(200, currentLine)
    invitation.pmses.length > 1 ? builder.addText('-----') : 'null'
    currentLine += addLine
  } */
  // currentLine += addSection

  builder.addPagePosition(0, currentLine)
  builder.addText('REGALO(S)')
  builder.addPagePosition(0, currentLine + 7)
  builder.addText('_______________________________________________')

  currentLine += addSection
  /* -- Box4 -- */
  builder.addPagePosition(0, currentLine)
  builder.addText('Descripcion')
  builder.addPagePosition(wPos_9, currentLine)
  builder.addText('Cantidad')

  currentLine += addLine

  for (let i = 0; i < invitation.gifts.length; i++) {
    builder.addPagePosition(0, currentLine)
    builder.addText(invitation.gifts[i].description)
    builder.addPagePosition(wPos_9, currentLine)
    builder.addText(invitation.gifts[i].quatity)
    currentLine += addLine
  }

  currentLine += addSection
  builder.addPagePosition(0, currentLine)
  builder.addText('NOTA')
  builder.addPagePosition(0, currentLine + 7)
  builder.addText('_______________________________________________')

  /* -- Box5 -- */
  currentLine += addSection
  builder.addPagePosition(0, currentLine)
  builder.addText('Invitado Por:')

  currentLine += addLine

 /*  invitation.userInvitations.map((value) => {
    builder.addPagePosition(0, currentLine)
    builder.addText(`${value.buser.employee.firstName} ${value.buser.employee.lastName}  ${value.buser.employee.code}`)
    currentLine += addLine
  })
 */
  currentLine += addLine * 2

  builder.addPagePosition(0, currentLine)
  builder.addText('Location')

  builder.addPagePosition(0, currentLine + addLine)
  builder.addText(invitation.location)

  builder.addPagePosition(wPos_6, currentLine)
  builder.addText('Fecha')
  builder.addPagePosition(wPos_6, currentLine + addLine)
  builder.addText(invitation.creationDate)

  builder.addPagePosition(wPos_10 + standar, currentLine)
  builder.addText('Hora')
  builder.addPagePosition(wPos_10 + standar, currentLine + addLine)
  builder.addText(invitation.creationTime)

  currentLine += addSection + 50

  // let a = 1
  /*  while (a < 101) {
     a = a + 1
     builder.addPagePosition(0, currentLine + a).addText('_______________________________________________')
   } */
  builder.addPagePosition(200, currentLine + addLine)
  builder.addTextDouble(true, false)
  invitation.callProspect ? builder.addText('LLAMAR') : builder.addText('NO LLAMAR')
  builder.addTextDouble(false, false)
  console.log(currentLine)
  // end page mode

  builder.addPageEnd()

  // paper control
  // builder.addFeedPosition(builder.FEED_PEELING);
  builder.addCut(builder.CUT_FEED)

  let request = builder.toString()
  console.log(request)
  let addressIP = printerIp
  let address = 'http://' + addressIP + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=10000'

  let epos = new epson.ePOSPrint(address)
  epos.send(request)
}
