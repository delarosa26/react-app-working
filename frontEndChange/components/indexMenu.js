import React, {Component} from 'react'
import {StyleSheet} from 'react-native'
import {
  Container,
  Content,
  Button,
  List,
  Item,
  Text
} from 'native-base'

import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class IndexMenu extends Component {
  static navigationOptions = {
    title: 'INICIO',
    headerStyle: {
      borderBottomWidth: 2,
      borderBottomColor: '#a0915a'
    },
    headerTitleStyle: {
      color: '#a0915a'
    },
    headerBackTitleStyle: {
      color: '#a0915a'
    },
    headerLeft: (
      <Text></Text>
    ),
    tabBarLabel: 'INICIO',
    tabBarIcon: ({tintColor}) => (<Icon name="home" style={[
      styles.iconSize, {
        color: tintColor
      }
    ]}/>)
  }
  render() {
    return (
      <Container style={styles.contentBackColor}>
        <Content >
          <List style={[styles.centerHeightItem]}>
            <Item style={[
              styles.inlineCenterItem, {
                borderBottomWidth: 0
              }
            ]}>
              <Button iconRight full light style={[
                styles.savebtn,
                styles.inlineItem, {
                  minWidth: 300
                }
              ]} onPress={() => this.props.navigation.navigate('CreateView')}>
                <Text style={styles.primarybtnText}>
                  CREAR INVITACION
                </Text>
                <Icon style={[
                  styles.creamIcon, {
                    marginRight: 0
                  }
                ]} name="recent-actors"/>
              </Button>
            </Item>

            <Item style={[
              styles.inlineCenterItem, {
                borderBottomWidth: 0
              }
            ]}>
              <Button iconRight full light style={[
                styles.savebtn,
                styles.inlineItem, {
                  minWidth: 300
                }
              ]} onPress={() => this.props.navigation.navigate('CompListView')}>
                <Text style={styles.primarybtnText}>
                  LISTADO INVITACIONES
                </Text>
                <Icon style={[
                  styles.creamIcon, {
                    marginRight: 0
                  }
                ]} name="view-list"/>
              </Button>
            </Item>

            <Item style={[
              styles.inlineCenterItem, {
                borderBottomWidth: 0
              }
            ]}>
              <Button iconRight full light style={[
                styles.savebtn,
                styles.inlineItem, {
                  maxWidth: 300
                }
              ]} onPress={() => this.props.navigation.navigate('Premanifiesto')}>
                <Text style={styles.primarybtnText}>
                  PRE-MANIFIESTO
                </Text>
                <Icon style={[
                  styles.creamIcon, {
                    marginRight: 0
                  }
                ]} name="insert-chart"/>
              </Button>
            </Item>
          </List>
        </Content>
      </Container>
    )
  }
}
