const React = require("react-native");

const {StyleSheet} = React;

export default {
  container: {
    backgroundColor: '#fff',
    borderRadius: 10,
    flex: 1
  },

  closeBtn: {
    color: '#fff',
    fontSize: 40,

  },
  btn:{
    marginTop:60,
    marginLeft: 'auto',
    marginBottom: -10
  },

  padding:{
    paddingLeft:15,
    paddingRight:15
  },

  item:{
    padding: 5,
    borderColor:'transparent',
    backgroundColor:'#f0eff4'
  },
  itemInside:{
    margin:5,
    borderColor:'#E8E8E8',
    borderBottomWidth:3,
    backgroundColor:'#fff'
  },
  itemIcon:{
    fontSize:20,
    color:'#222',
    margin:5
  },
  searchText:{
    color:'#222',
  },
  giftBtn:{
    height: 63
  },

  giftIcon:{
    fontSize:20,
    color:'#222',
  },

  giftbtn:{
    justifyContent:'flex-start',
    paddingLeft:20

  }
}
