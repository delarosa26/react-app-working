import React, {Component} from 'react'
import {StyleSheet} from 'react-native';

import {
  ListItem,
  Item,
  Input,
  Body,
  Text,
  Left,
  Right,
  Label,
  View,
  Content,
  Button,
  SwipeRow
} from 'native-base'
import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Popup from 'react-native-popup'
import GiftsAdd from './giftsAdd'

export default class PMSform extends Component {
  onPressHandle() {
		this.popup.alert(1, 'two', '10 messages at most');
	}
  render() {
    return (
      <View>
        <SwipeRow
           rightOpenValue={-75}
           body={
             <Item style={{borderColor:'transparent'}}>
             <Icon name="perm-contact-calendar" style={[styles.creamIcon, {marginLeft:15}]}/>
             <Text style={styles.graycolor}>
               GUILLERMO LOPEZ
            </Text>
            <Text style={{
              marginLeft: 30
            }}>HABITACION
             81101
            </Text>
            </Item>
           }
           right={
             <Button danger onPress={this.onPressHandle.bind(this)}>
               <Icon active name="delete-forever" style={styles.whiteIcon}/>
                <Popup ref={popup => this.popup = popup }/>
             </Button>
           }
       />

      </View>
    )
  }
}
