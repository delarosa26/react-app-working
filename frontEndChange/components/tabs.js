import React, {Component} from 'react'
import {StyleSheet} from 'react-native';

import {TabNavigator} from 'react-navigation'
import CompListView from './listView'
import CreateView from './createView'
import IndexMenu from './indexMenu'

const TabsBar = TabNavigator({
  IndexMenu: {
    screen: IndexMenu
  },
  CreateView: {
    screen: CreateView
  },
  CompListView: {
    screen: CompListView
  }
}, {
  animationEnabled: true,
  scrollEnabled:true,
  tabBarOptions: {
    inactiveTintColor:'#a0915a',
    activeTintColor: '#222',
    labelStyle: {
      fontSize: 14,
      color:"#505050"
    }
  }
})

export default TabsBar;
