import React, {Component} from 'react'
import {StyleSheet} from 'react-native';
import {
  Container,
  Content,
  Form,
  List,
  ListItem,
  Separator,
  Item,
  Input,
  Body,
  Text,
  Left,
  Right,
  Label,
  View,
  Button
} from 'native-base'
import styles from './styles'
import modalstyle from './modalstyle'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class PMScreate extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'AGREGAR PMS',
      headerTitleStyle: {
        color: '#a0915a'
      },
      headerRight: (
        <Button transparent dark medium>
          <Text style={styles.addPmstxt}>AGREGAR</Text>
        </Button>
      ),

      headerLeft: (
        <Button transparent dark medium onPress={() => navigation.goBack(null)}>
          <Icon name='keyboard-arrow-left' style={styles.topIcon}/>
          <Text style={styles.addPmstxt}>CANCELAR</Text>
        </Button>
      )

    }
  }

  render() {
    return (
      <Container style={modalstyle.container}>
        <Content style={modalstyle.background} padder>
          <Form >
            <List>
              <Separator></Separator>
              <Item style={modalstyle.padding}>
                <Label>HABITACION
                </Label>
                <Input value="81101"/>
                <Icon name="search" style={styles.creamIcon}/>
              </Item>
              <ListItem icon>
                <Icon name="sim-card-alert" style={styles.creamIcon}/>

                <Text>
                  CLIENTE NO ENCONTRADO / POR FAVOR COMPLETE LOS CAMPOS
                </Text>

              </ListItem>
              <Item style={modalstyle.item}>
                <Input style={modalstyle.itemInside} placeholder="NOMBRE"/>
                <Input style={modalstyle.itemInside} placeholder="APELLIDO"/>
              </Item>
              <Item style={modalstyle.item}>
                <Input style={modalstyle.itemInside} placeholder="HABITACION"/>
                <Input style={modalstyle.itemInside} placeholder="CHECK-IN "/>
                <Input style={modalstyle.itemInside} placeholder="CHECK-OUT"/>
              </Item>
              <Item style={modalstyle.item}>
                <Input style={modalstyle.itemInside} placeholder="HOTEL"/>
                <Input style={modalstyle.itemInside} placeholder="TOUR OPERADOR "/>
                <Input style={modalstyle.itemInside} placeholder="PAIS RESIDENCIA"/>
              </Item>
            </List>
          </Form>
        </Content>
      </Container>
    )
  }
}
