import React, {Component} from 'react'
import {View, Text, Animated} from 'react-native'
import chartStyles from './chartStyles'

export default class Charts extends Component {
  render() {
    const title = 'Hello'
    return (
      <View style={chartStyles.container}>

        <View style={chartStyles.item}>
          <Text style={chartStyles.label}>5:50</Text>
          <View style={chartStyles.data}>
            {< Animated.View style = {
              [
                chartStyles.bar,
                chartStyles.ola, {
                  width: 80
                }
              ]
            } />}
            <Text style={chartStyles.dataNumber}>2</Text>
          </View>
        </View>

        <View style={chartStyles.item}>
          <Text style={chartStyles.label}>4:27</Text>
          <View style={chartStyles.data}>
            {< Animated.View style = {
              [
                chartStyles.bar,
                chartStyles.ola, {
                  width: 40
                }
              ]
            } />}
            <Text style={chartStyles.dataNumber}>1</Text>
          </View>
        </View>

        <View style={chartStyles.item}>
          <Text style={chartStyles.label}>1:00</Text>
          <View style={chartStyles.data}>
            {< Animated.View style = {
              [
                chartStyles.bar,
                chartStyles.ola, {
                  width: 120
                }
              ]
            } />}
            <Text style={chartStyles.dataNumber}>3</Text>
          </View>
        </View>

        <View style={chartStyles.item}>
          <Text style={chartStyles.label}>12:00</Text>
          <View style={chartStyles.data}>
            {< Animated.View style = {
              [
                chartStyles.bar,
                chartStyles.ola, {
                  width: 200
                }
              ]
            } />}
            <Text style={chartStyles.dataNumber}>5</Text>
          </View>
        </View>

        <View style={chartStyles.item}>
          <Text style={chartStyles.label}>11:00</Text>
          <View style={chartStyles.data}>
            {< Animated.View style = {
              [
                chartStyles.bar,
                chartStyles.ola, {
                  width: 80
                }
              ]
            } />}
            <Text style={chartStyles.dataNumber}>2</Text>
          </View>
        </View>

        <View style={chartStyles.item}>
          <Text style={chartStyles.label}>11:00</Text>
          <View style={chartStyles.data}>
            {< Animated.View style = {
              [
                chartStyles.bar,
                chartStyles.ola, {
                  width: 400
                }
              ]
            } />}
            <Text style={chartStyles.dataNumber}>10</Text>
          </View>
        </View>
      </View>
    )
  }
}
