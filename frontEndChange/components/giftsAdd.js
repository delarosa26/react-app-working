import React, {Component} from 'react'
import {StyleSheet} from 'react-native';
import {
  Container,
  Content,
  Form,
  List,
  ListItem,
  Separator,
  Item,
  Input,
  Body,
  Text,
  Left,
  Right,
  Label,
  View,
  Button
} from 'native-base'
import styles from './styles'
import modalstyle from './modalstyle'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class GiftsAdd extends Component {

  static navigationOptions = ({navigation}) => {
    return {
      title: 'REGALOS',
      headerTitleStyle: {
        color: '#a0915a'
      },
      headerRight: (
        <Button transparent dark medium>
          <Text style={styles.addPmstxt}>AGREGAR</Text>
        </Button>
      ),

      headerLeft: (
        <Button transparent dark medium onPress={() => navigation.goBack(null)}>
          <Icon name='keyboard-arrow-left' style={styles.topIcon}/>
          <Text style={styles.addPmstxt}>CANCELAR</Text>
        </Button>
      )

    }
  }
  render() {
    return (
      <Container style={modalstyle.container}>
        <Content style={modalstyle.background} padder>
          <Form >
            <List>
              <Separator>
              </Separator>
              <Item style={modalstyle.padding}>
                <Label>CODIGO REGALO
                </Label>
                <Input value="SQD81101"/>
                <Icon name="search" style={styles.creamIcon}/>
              </Item>
              <Item style={modalstyle.item}>
                <Left>
                  <Button full style={[{marginLeft:5}, modalstyle.itemInside, modalstyle.giftBtn, styles.inlineItem, modalstyle.giftbtn]}>
                    <Icon name="card-giftcard" style={styles.creamIcon}/>
                    <Text style={modalstyle.searchText}>
                      20% DE DESCUENTOS EN SERVICIOS
                    </Text>
                  </Button>

                </Left>
                <Item style={modalstyle.itemInside}>
                  <Button large dark transparent style={styles.inlineItem}>
                    <Icon style={styles.iconSize} name="remove"/>
                  </Button>
                  <Text style={[styles.inlineItem]}>
                    2
                  </Text>
                  <Button large dark transparent style={styles.inlineItem}>
                    <Icon style={styles.iconSize} name="add"/>
                  </Button>
                </Item>
              </Item>

              <Item style={[modalstyle.item]}>
                <Left>
                  <Button full style={[{marginLeft:5}, modalstyle.itemInside, modalstyle.giftBtn, styles.inlineItem, modalstyle.giftbtn]}>
                    <Icon name="card-giftcard" style={styles.creamIcon}/>
                    <Text style={modalstyle.searchText}>
                      NOCHE DE SPA
                    </Text>
                  </Button>

                </Left>
                <Item style={modalstyle.itemInside}>
                  <Button large dark transparent style={styles.inlineItem}>
                    <Icon style={styles.iconSize} name="remove"/>
                  </Button>
                  <Text style={[styles.inlineItem]}>
                    3
                  </Text>
                  <Button large dark transparent style={styles.inlineItem}>
                    <Icon style={styles.iconSize} name="add"/>
                  </Button>
                </Item>
              </Item>

              <Item style={[modalstyle.item]}>
                <Left>
                  <Button full style={[{marginLeft:5}, modalstyle.itemInside, modalstyle.giftBtn, styles.inlineItem, modalstyle.giftbtn]}>
                    <Icon name="card-giftcard" style={styles.creamIcon}/>
                    <Text style={modalstyle.searchText}>
                      50% DE DESCUENTO EN TRAGOS
                    </Text>
                  </Button>

                </Left>
                <Item style={modalstyle.itemInside}>
                  <Button large dark transparent style={styles.inlineItem}>
                    <Icon style={styles.iconSize} name="remove"/>
                  </Button>
                  <Text style={[styles.inlineItem]}>
                    1
                  </Text>
                  <Button large dark transparent style={styles.inlineItem}>
                    <Icon style={styles.iconSize} name="add"/>
                  </Button>
                </Item>
              </Item>
            </List>
          </Form>
        </Content>
      </Container>
    )
  }
}
