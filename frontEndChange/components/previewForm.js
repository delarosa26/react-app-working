import React, {Component} from 'react'
import {StyleSheet} from 'react-native';
import {
  Container,
  Content,
  Form,
  List,
  ListItem,
  Separator,
  Item,
  Input,
  Body,
  Text,
  Left,
  Right,
  Label,
  View,
  Button,
  Grid,
  Col,
  Row
} from 'native-base'
import styles from './styles'
import modalstyle from './modalstyle'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class PreviewForm extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: 'RESUMEN INVITACION',
      headerTitleStyle: {
        color: '#a0915a'
      },
      headerLeft: (
        <Button transparent dark medium onPress={() => navigation.goBack(null)}>
          <Icon name='keyboard-arrow-left' style={styles.topIcon}/>
          <Text style={styles.addPmstxt}>VOLVER</Text>
        </Button>
      )
    }
  }
  render() {
    return (
      <Container style={modalstyle.container}>
        <Content style={modalstyle.background} padder>
          <List>
            <Separator>
              <Text>
                INFORMACION CLIENTE
              </Text>
            </Separator>
            <Item style={[{
              borderColor: 'transparent'
            },
            styles.inlineItem]}>
              <Icon name="perm-contact-calendar" style={[
                styles.creamIcon, {
                  marginLeft: 15
                }
              ]}/>
              <Text style={styles.secondarycolor}>
                GUILLERMO LOPEZ
              </Text>
              <Text style={{
                marginLeft: 30
              }}>HABITACION 81101
              </Text>
            </Item>
            <Separator>
              <Text>
                DETALLES EVENTO
              </Text>
            </Separator>
            <Item style={styles.inlineItem}>
              <Icon name="date-range" style={[
                styles.creamIcon, {
                  marginLeft: 15
                }
              ]}/>
              <Text style={styles.secondarycolor}>
                FECHA: 09/01/2014
              </Text>
              <Right>
                <Text style={styles.graycolor}>
                  HORA ENTRADA: 5:00PM
                </Text>
              </Right>
            </Item>
            <Item style={styles.inlineItem}>
              <Icon name="info" style={[
                styles.creamIcon, {
                  marginLeft: 15
                }
              ]}/>
              <Text style={styles.secondarycolor}>
                IDIOMA: SPANISH
              </Text>
              <Right>
                <Text style={styles.graycolor}>
                  ADULTOS: 4 / NIÑOS: 2
                </Text>
              </Right>
            </Item>
            <Item style={styles.inlineItem}>
              <Icon name="location-on" style={[
                styles.creamIcon, {
                  marginLeft: 15
                }
              ]}/>
              <Text style={styles.secondarycolor}>
                PICKUP: LOBBY
              </Text>
              <Right>
                <Text style={styles.graycolor}>
                  HORA PICKUP: 4:45PM
                </Text>
              </Right>
            </Item>
            <Separator style={styles.gitTitle}>
              <Text >
                DESCRIPCION REGALO
              </Text>
              <Text >
                CANTIDAD
              </Text>
            </Separator>
            <Item style={styles.inlineItem}>
              <Icon name="card-giftcard" style={[
                styles.creamIcon, {
                  marginLeft: 15
                }
              ]}/>
              <Text style={styles.secondarycolor}>
                POLOCHE
              </Text>
              <Right>
                <Text style={styles.graycolor}>
                  152
                </Text>
              </Right>
            </Item>

            <Separator style={styles.gitTitle}>
              <Text >
                INVITADO POR
              </Text>
              <Text >
                CODIGO OPC
              </Text>
            </Separator>
            <Item style={styles.inlineItem}>
              <Icon name="person-pin-circle" style={[
                styles.creamIcon, {
                  marginLeft: 15
                }
              ]}/>
              <Text style={styles.secondarycolor}>
                JULIANA PEREZ
              </Text>
              <Right>
                <Text style={styles.graycolor}>
                  45689
                </Text>
              </Right>
            </Item>
            <Item style={styles.inlineItem}>

              <Text style={styles.secondarycolor}>
                HORA CREACION DE LA INVITACION: 3.00PM
              </Text>
            </Item>
            <Item style={styles.inlineItem}>

              <Text style={styles.secondarycolor}>
                COMENTARIO
              </Text>
              <Right>
                <Text style={styles.graycolor}>
                  Text sample about it
                </Text>
              </Right>
            </Item>
          </List>
        </Content>
      </Container>
    )
  }
}
