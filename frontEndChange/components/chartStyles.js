const React = require("react-native");

const { StyleSheet, Dimensions } = React;

export default {
  bar:{
    alignSelf: 'center',
    borderRadius: 8,
    height: 12,
    marginRight: 5
  },
  container: {
    flexDirection: 'column',
    marginTop: 6
  },
  ola:{
    backgroundColor:'#a0915a'
  },
  item: {
    flexDirection: 'column',
    marginBottom: 5,
    paddingHorizontal: 10
  },
  label: {
    color: '#222',
    flex: 1,
    fontSize: 13,
    position: 'relative',
    top: 2,
    marginTop: 10,
    marginBottom: 3,
    marginLeft: 5
  },
  data: {
    flex: 2,
    flexDirection: 'row'
  },
  dataNumber: {
    color: '#606060',
    fontSize: 13
  },
}
