import React, {Component} from 'react'

import {StyleSheet} from 'react-native';
import {
  Body,
  Container,
  Content,
  Grid,
  Col,
  Row,
  Item,
  Input,
  Button,
  Text,
  Right,
  H3,
  H1,
  CardItem,
  Card,
  Separator,
  Left
} from 'native-base';
import styles from './styles'
import Charts from './charts'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class Premanifiesto extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: 'PRE-MANIFIESTO',
      headerTitleStyle: {
        color: '#a0915a'
      },
      headerLeft: (
        <Button transparent dark medium onPress={() => navigation.goBack(null)}>
          <Icon name='keyboard-arrow-left' style={styles.topIcon}/>
          <Text style={styles.addPmstxt}>VOLVER</Text>
        </Button>
      )
    }
  }
  render() {
    return (
      <Container style={styles.contentBackColor}>
        <Content>
          <Item style={{
            padding: 20
          }}>
            <Icon name='insert-chart' style={styles.topIcon}/>
            <Right>
              <H3 style={styles.addPmsbtn}>
                PRE-MANIFIESTO
              </H3>
              <Text style={styles.addPmsbtn}>
                OCT 6, 2017
              </Text>

            </Right>
          </Item>

          <Item style={[styles.inlineCenterItem, styles.paddingItem]}>
            <Grid>
              <Separator>
                <Text>
                  REPORTE INVITACIONES PRE-MANIFESTADAS
                </Text>
              </Separator>
              <Row>
                <Card>
                  <CardItem style={[styles.centerItem, styles.headerColor]} header bordered>
                    <Text style={styles.circleText}>TOTAL</Text>
                  </CardItem>
                  <CardItem style={styles.centerItem}>
                    <H1 style={styles.addPmstxt}>
                      13
                    </H1>
                  </CardItem>
                </Card>

                <Card>
                  <CardItem style={[styles.centerItem, styles.headerColor]} header bordered>
                    <Text style={styles.circleText}>REPROGRAMADAS</Text>
                  </CardItem>
                  <CardItem style={styles.centerItem}>
                    <H1 style={styles.addPmstxt}>
                      0
                    </H1>
                  </CardItem>
                </Card>
                <Card>
                  <CardItem style={[styles.centerItem, styles.headerColor]} header bordered>
                    <Text style={styles.circleText}>ADULTOS</Text>
                  </CardItem>
                  <CardItem style={styles.centerItem}>
                    <H1 style={styles.addPmstxt}>
                      15
                    </H1>
                  </CardItem>
                </Card>

                <Card>
                  <CardItem style={[styles.centerItem, styles.headerColor]} header bordered>
                    <Text style={styles.circleText}>NIÑOS</Text>
                  </CardItem>
                  <CardItem style={styles.centerItem}>
                    <H1 style={styles.addPmstxt}>
                      1
                    </H1>
                  </CardItem>
                </Card>
              </Row>
              <Row>
                <Card>
                  <CardItem header bordered style={styles.headerColor}>
                    <Left>
                      <Icon name="card-giftcard" style={[
                        styles.creamIcon, {
                          marginLeft: 10
                        }
                      ]}/>
                      <Text style={styles.primarybtnText}>
                        REGALOS
                      </Text>
                    </Left>
                    <Right>
                      <Text style={[
                        styles.subTxt, {
                          fontWeight: 'bold'
                        }
                      ]}>
                        TOTAL: 4
                      </Text>
                      <Text style={styles.subTxt}>
                        AVG: 2.2
                      </Text>
                      <Text style={styles.subTxt}>
                        MAX: 3
                      </Text>
                    </Right>
                  </CardItem>
                  <CardItem style={styles.primaryBackground}>
                    <Left>
                      <Text style={styles.subTxt}>
                        DESCRIPCION
                      </Text>
                    </Left>
                    <Right>
                      <Text style={styles.subTxt}>
                        CANTIDAD
                      </Text>
                    </Right>
                  </CardItem>
                  <CardItem bordered>
                    <Left>
                      <Text style={styles.secondarycolor}>
                        BONO DE $80 USD CON SOL TOUR
                      </Text>
                    </Left>
                    <Right>
                      <Text style={styles.secondarycolor}>
                        2
                      </Text>
                    </Right>
                  </CardItem>
                  <CardItem bordered>
                    <Left>
                      <Text style={styles.secondarycolor}>
                        PAQUETE DOMINICANO NQ
                      </Text>
                    </Left>
                    <Right>
                      <Text style={styles.secondarycolor}>
                        3
                      </Text>
                    </Right>
                  </CardItem>
                  <CardItem bordered>
                    <Left>
                      <Text style={styles.secondarycolor}>
                        PAQUETE DOMINICANO NQ
                      </Text>
                    </Left>
                    <Right>
                      <Text style={styles.secondarycolor}>
                        3
                      </Text>
                    </Right>
                  </CardItem>
                </Card>
              </Row>

              <Row>
                <Card>
                  <CardItem header bordered style={styles.headerColor}>
                    <Left>
                      <Icon name="person-pin-circle" style={[
                        styles.creamIcon, {
                          marginLeft: 10
                        }
                      ]}/>
                      <Text style={styles.primarybtnText}>
                        EMPLEADOS
                      </Text>
                    </Left>
                    <Right>
                      <Text style={[
                        styles.subTxt, {
                          fontWeight: 'bold'
                        }
                      ]}>
                        TOTAL: 3
                      </Text>
                      <Text style={styles.subTxt}>
                        AVG: 4.7
                      </Text>
                      <Text style={styles.subTxt}>
                        MAX: 11
                      </Text>
                    </Right>
                  </CardItem>
                  <CardItem style={styles.primaryBackground}>
                    <Left>
                      <Text style={styles.subTxt}>
                        NOMBRE
                      </Text>
                    </Left>
                    <Right>
                      <Text style={styles.subTxt}>
                        CANTIDAD
                      </Text>
                    </Right>
                  </CardItem>
                  <CardItem bordered>
                    <Left>
                      <Text style={styles.secondarycolor}>
                        ADRIAN
                      </Text>
                    </Left>
                    <Right>
                      <Text style={styles.secondarycolor}>
                        11
                      </Text>
                    </Right>
                  </CardItem>
                  <CardItem bordered>
                    <Left>
                      <Text style={styles.secondarycolor}>
                        RODRIGO A.
                      </Text>
                    </Left>
                    <Right>
                      <Text style={styles.secondarycolor}>
                        2
                      </Text>
                    </Right>
                  </CardItem>
                  <CardItem bordered>
                    <Left>
                      <Text style={styles.secondarycolor}>
                        CHLOE ELOISE
                      </Text>
                    </Left>
                    <Right>
                      <Text style={styles.secondarycolor}>
                        1
                      </Text>
                    </Right>
                  </CardItem>
                </Card>
              </Row>
              <Row>
                <Col size={1}>
                  <Card>
                    <CardItem header bordered style={styles.headerColor}>
                      <Left>
                        <Icon name="people" style={[
                          styles.creamIcon, {
                            marginLeft: 10
                          }
                        ]}/>
                        <Text style={styles.primarybtnText}>
                          OLA
                        </Text>
                      </Left>
                      <Right>
                        <Text style={[
                          styles.subTxt, {
                            fontWeight: 'bold'
                          }
                        ]}>
                          TOTAL: 6
                        </Text>
                        <Text style={styles.subTxt}>
                          AVG: 2.2
                        </Text>
                        <Text style={styles.subTxt}>
                          MAX: 5
                        </Text>
                      </Right>
                    </CardItem>
                    <CardItem>
                      <Charts/>
                    </CardItem>
                  </Card>
                </Col>
              </Row>
            </Grid>
          </Item>
        </Content>
      </Container>
    );
  }
}
