import React, {Component} from 'react'

import {StyleSheet} from 'react-native';
import {
  Header,
  Item,
  Input,
  Button,
  Text,
  Right
} from 'native-base';
import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class searchHeader extends Component {
  render() {
    return (
      <Header searchBar style={searchStyles.brandSearch}>
        <Item style={[searchStyles.noBorderItem, styles.inlineCenterItem]}>
          <Input style={searchStyles.searchinput} placeholder="BUSCAR POR HABITACION"/>
          <Right>
            <Button transparent light>
              <Icon style={searchStyles.iconSize} active name="search"/>
            </Button>
          </Right>
        </Item>
      </Header>
    );
  }
}

const searchStyles = StyleSheet.create({
  brandSearch: {
    backgroundColor: '#fff',
  },
  noBorderItem: {
    borderColor: 'transparent',
    borderWidth: 0,
    backgroundColor: '#FFF'
  },
  iconSize: {
    fontSize: 25,
    color: '#a0915a'
  },
  searchinput: {
    borderWidth: 0,
    backgroundColor: '#fff',
    marginLeft:-20
  }
})
