import React, {Component} from 'react'
import {StyleSheet} from 'react-native'
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Left,
  Right,
  Body,
  Text,
  Item,
  Form,
  List,
  ListItem,
  Separator,
  Grid,
  Col,
  Row,
  Input,
  Switch
} from 'native-base'

import styles from './styles'
import Flag from 'react-native-flags'
import Icon from 'react-native-vector-icons/MaterialIcons'
import PMSform from './PMSform'

export default class CreateView extends Component {
  static navigationOptions = {
    title: 'INVITACION',
    headerStyle: {
      borderBottomWidth: 2,
      borderBottomColor: '#a0915a'
    },
    headerTitleStyle: {
      color: '#a0915a'
    },
    headerBackTitleStyle: {
      color: '#a0915a'
    },
    headerLeft: (
      <Text> </Text>
    ),
    tabBarLabel: 'INVITACION',
    tabBarIcon: ({ tintColor }) => ( <Icon name="recent-actors" style={[styles.iconSize, {color: tintColor}]}/>)
  }

  render() {
    return (
      <Container style={styles.contentBackColor}>
        <Content>
          <Item style={styles.addPms}>
            <Button transparent style={styles.addPmsbtn} onPress={() => this.props.navigation.navigate('PMScreate')}>
              <Text style={styles.addPmstxt}>
                AGREGAR PMS
              </Text>
              <Icon style={styles.secondaryIcon} name="person-add"/>
            </Button>
          </Item>
          <Form>
            <List>
              <PMSform/>
              <Separator bordered>
                <Text style={styles.centerText}>
                  DETALLES DEL EVENTO
                </Text>
              </Separator>
              <Item style={styles.inlineCenterItem}>
                <Button large transparent dark style={[styles.columnItem, styles.btnText]}>
                  <Text>
                    SEP
                  </Text>
                  <Text style={styles.bigFont}>
                    19
                  </Text>
                </Button>
                <Button large transparent dark style={[styles.columnItem, styles.btnText, styles.activeDate]}>
                  <Text>
                    SEP
                  </Text>
                  <Text style={styles.bigFont}>
                    20
                  </Text>
                </Button>

                <Button large transparent dark style={[styles.columnItem, styles.btnText]}>
                  <Text>
                    SEP
                  </Text>
                  <Text style={styles.bigFont}>
                    21
                  </Text>
                </Button>
              </Item>

              <Item style={styles.inlineCenterItem}>
                <Button large transparent dark style={[styles.offset, styles.columnItem, styles.activeFlag, styles.activeDate]}>
                  <Flag style={styles.flag} code="IT" size={48} type="flat"></Flag>
                </Button>
                <Button large transparent dark style={[styles.offset, styles.columnItem, styles.inactiveFlag]}>
                  <Flag style={styles.flag} code="ES" size={48} type="flat"></Flag>
                </Button>
                <Button large transparent dark style={[styles.offset, styles.columnItem, styles.inactiveFlag]}>
                  <Flag style={styles.flag} code="FR" size={48} type="flat"></Flag>
                </Button>
                <Button large transparent dark style={[styles.offset, styles.columnItem, styles.inactiveFlag]}>
                  <Flag style={styles.flag} code="PT" size={48} type="flat"></Flag>
                </Button>
                <Button large transparent dark style={[styles.offset, styles.columnItem, styles.inactiveFlag]}>
                  <Flag style={styles.flag} code="GB" size={48} type="flat"></Flag>
                </Button>
              </Item>

              <Item style={styles.inlineCenterItem}>
                <Button large transparent dark style={[styles.columnItem, styles.btnText]}>
                  <Text>
                    9:00
                  </Text>
                  <Text style={styles.noteFont} note>
                    0/30
                  </Text>
                </Button>
                <Button large transparent dark style={[styles.columnItem, styles.btnText, styles.activeDate]}>
                  <Text>
                    11:00
                  </Text>
                  <Text style={styles.noteFont} note>
                    0/30
                  </Text>
                </Button>
                <Button large transparent dark style={[styles.columnItem, styles.btnText]}>
                  <Text>
                    12:00
                  </Text>
                  <Text style={styles.noteFont} note>
                    0/30
                  </Text>
                </Button>
              </Item>

              <Grid>
                <Col size={3}>
                  <Separator>
                    <Text style={styles.centerText}>
                      ADULTOS
                    </Text>
                  </Separator>
                  <Item style={styles.inlineCenterItem}>
                    <Button large dark transparent style={styles.inlineItem}>
                      <Icon style={styles.iconSize} name="remove"/>
                    </Button>
                    <Text style={[styles.inlineItem, styles.bigFont]}>
                      2
                    </Text>
                    <Button large dark transparent style={styles.inlineItem}>
                      <Icon style={styles.iconSize} name="add"/>
                    </Button>
                  </Item>
                </Col>
                <Col size={3} style={{
                  borderLeftWidth: StyleSheet.hairlineWidth,
                  borderColor: '#ccc'
                }}>
                  <Separator>
                    <Text style={styles.centerText}>
                      NIÑOS
                    </Text>
                  </Separator>
                  <Item style={styles.inlineCenterItem}>
                    <Button large dark transparent style={styles.inlineItem}>
                      <Icon style={styles.iconSize} name="remove"/>
                    </Button>
                    <Text style={[styles.inlineItem, styles.bigFont]}>
                      5
                    </Text>
                    <Button large dark transparent style={styles.inlineItem}>
                      <Icon style={styles.iconSize} name="add"/>
                    </Button>
                  </Item>
                </Col>

              </Grid>
              <ListItem icon style={{
                borderWidth: 0
              }}>
                <Left>
                  <Button transparent>
                    <Icon style={styles.iconSize} color="#a0915a" name="card-giftcard"/>
                  </Button>
                </Left>
                <Body>
                  <Text>
                    REGALOS
                  </Text>
                </Body>
                <Right>
                  <Button transparent onPress={() => this.props.navigation.navigate('GiftsAdd')}>
                    <Icon style={styles.creamIcon} name="library-add"/>
                  </Button>
                  <Button transparent>
                    <Icon style={styles.creamIcon} name="delete-forever"/>
                  </Button>
                  <Button transparent>
                    <Icon style={styles.creamIcon} name="info"/>
                  </Button>
                </Right>
              </ListItem>
              <ListItem itemDivider></ListItem>
              <Item>
                <Input style={styles.textarea} placeholder='Comentarios' multiline={true} numberOfline={3}/>
              </Item>
              <Item style={styles.inlineCenterItem}>
                <Text>ACEPTA LLAMADA RECORDATORIA</Text>
                <Switch style={{
                  marginLeft: 10
                }} value={true}/>
              </Item>
            </List>
          </Form>
          <Item style={[styles.inlineCenterItem, styles.primaryItem]}>
            <Button iconRight style={[styles.savebtn, styles.inlineItem]}>
              <Text style={styles.primarybtnText}>
                GUARDAR INVITACION
              </Text>
              <Icon style={[
                styles.creamIcon, {
                  marginRight: 10
                }
              ]} name="save"/>
            </Button>
          </Item>
        </Content>
      </Container>
    );
  }
}
