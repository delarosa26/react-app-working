const React = require("react-native");

const { StyleSheet, Dimensions } = React;

export default {
  contentBackColor:{
    backgroundColor: '#fff'
  },
  inlineItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 20,
    alignItems: "center"
  },
  inlineBtn: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  inlineCenterItem: {
    flexDirection: "row",
    justifyContent: "center",
    padding: 19,
    alignItems: "center"
  },
  inlineItem: {
    margin: 0,
    padding: 15
  },
  centerItem:{
    justifyContent:'center',
    alignItems:'center'
  },
  centerHeightItem:{
    justifyContent:'center',
    alignItems:'center',
    minHeight:400,
  },
  columnItem: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: 'column',
  },
  flag:{
    margin: 10,
    minHeight: 40
  },
  inactiveFlag:{
    opacity: 0.6
  },
  activaFlag:{
    opacity:1,
    borderColor:'#a0915a',
    borderWidth:StyleSheet.hairlineWidth,
    padding:0
  },
  offset:{
    margin:5
  },
  bigFont: {
    fontSize: 25
  },
  rightText: {
    textAlign: 'right',
    marginRight: 10
  },
  centerText: {
    textAlign: 'center'
  },
  content: {
    minHeight: 'auto'
  },
  creamIcon: {
    marginRight: 10,
    color: '#a0915a',
    fontSize: 25
  },
  iconSize: {
    fontSize: 30
  },
  textarea: {
    marginLeft: 10,
    minHeight: 78
  },
  daybtn: {
    borderColor: '#a0915a',
    borderBottomWidth: 2,
    borderTopWidth: 2
  },
  primarycolor: {
    color: '#a0915a'
  },
  primaryBackground: {
    backgroundColor: '#E8E8E8'
  },
  primaryItem:{
    backgroundColor: '#a0915a',
    marginLeft:0
  },
  primarybtnText:{
    fontWeight: 'bold',
    color:'#a0915a'
  },
  secondarycolor: {
    color: '#222'
  },
  graycolor: {
    color: '#A9A9A9'
  },
  whiteIcon:{
    color:"#fff",
    fontSize:25
  },
  menuIcon:{
    fontSize: 35,
    color:'#a0915a'
  },
  topIcon:{
    color:'#a0915a',
    fontSize:25,
    marginRight:20
  },
  activemenuIcon:{
    fontSize: 35,
    color:'#222'
  },
  savebtn: {
    borderColor: '#a0915a',
    borderWidth: 2,
    backgroundColor:'#fff'
  },
  brandLine: {
    borderBottomWidth: 2,
    borderBottomColor: '#a0915a'
  },
  activeDate:{
    borderColor:'#a0915a',
    borderWidth:StyleSheet.hairlineWidth,
  },
  btnText:{
    padding:0,
    minHeight: 75,
    marginLeft:20,
    marginRight:20,
  },
  noteFont:{
    fontSize: 17
  },
  addPms:{
    backgroundColor: '#f0eff4',
    marginLeft:0
  },
  addPmsbtn:{
    marginLeft:'auto',
    marginRight: 20,
  },
  secondaryIcon:{
    fontSize:25,
    color:'#222'
  },
  addPmstxt:{
    fontWeight:'bold',
    color:'#222'
  },
  topIcon:{
    fontSize:35,
    color:'#a0915a',
    marginLeft:0,
    marginRight:-20
  },
  listIcon:{
    color:'#a0915a',
    marginRight: 5,
    fontSize: 25
  },
  listText:{
    color:"#606060"
  },
  stateText:{
    fontSize:12,
    color:'#282828'
  },
  circle:{
    width:8,
    height:8,
    borderRadius:4,
    backgroundColor:'#a0915a'
  },
  circleText:{
    fontSize:15,
    fontWeight:'bold',
    color:'#a0915a'
  },
  activeState:{
    backgroundColor:'#E0E0E0'
  }
}
