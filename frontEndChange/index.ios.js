import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet
} from 'react-native';
import styles from './components/styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import SignIn from './components/signIn'
import CompListView from './components/listView'
import CreateView from './components/createView'
import PMScreate from './components/PMScreate'
import IndexMenu from './components/indexMenu'
import GiftsAdd from './components/giftsAdd'
import TabsBar from './components/tabs'
import PreviewForm from './components/previewForm'
import Premanifiesto from './components/premanifiesto'
import ComponentWithData from './components/listView'
import { StackNavigator, TabNavigator } from 'react-navigation'

import {
  ApolloProvider,
  ApolloClient,
  createNetworkInterface
 } from 'react-apollo'

const networkInterface = createNetworkInterface({
  uri: 'http://localhost:3000/graphql',
});

const client = new ApolloClient({ networkInterface })

const Router = StackNavigator({
  Login:{screen:SignIn},
  IndexMenu: { screen: IndexMenu },
  CompListView: { screen: CompListView },
  CreateView: { screen: CreateView },
  PMScreate: { screen: PMScreate },
  GiftsAdd: { screen: GiftsAdd },
  PreviewForm: { screen: PreviewForm },
  TabsBar: { screen: TabsBar },
  Premanifiesto: { screen: Premanifiesto},
  ComponentWithData: {screen: ComponentWithData}
})



const frontend = () => (
  <ApolloProvider client={client}>
      <Router />
  </ApolloProvider>
)

AppRegistry.registerComponent('frontend', () => frontend);
